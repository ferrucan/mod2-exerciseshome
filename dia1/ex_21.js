const PASS_NOTE = 5;
const NUMBER_OF_DECIMALS = 2;

function getTextualGrade(grade) {
    if ((grade >= 5) && (grade < 7) ) {
        return 'APROBADO';
    } else if ((grade>=7) && (grade < 9)) {
        return 'NOTABLE';
    } else if ( (grade>=9) && (grade <= 10)) {
        return 'SOBRESALIENTE';
    } else {
        return 'SUSPENSO';
    }
}

function calculateAverageGrade(grades) {
    let sum_grades = 0;

    for (let grade of grades) {
        sum_grades = sum_grades + grade;
    }

    return sum_grades / grades.length;
}

function printCard(name, grade, textGrade) {
    const LATERAL_WHITESPACES = 2;

    // first and last lines
    const header = '*'.repeat(name.length + LATERAL_WHITESPACES * 2);
    const footer = header;

    const line_size = name.length + LATERAL_WHITESPACES;

    const formatInnerLine = (text, maxLength) => {
        const innerLength = maxLength - 2;
        const prefix = ' '.repeat((line_size - text.length)/2);
        const suffix = ' '.repeat(innerLength - prefix.length - text.length);
    
        return `*${prefix}${text}${suffix}*`;
    }

    const average_grade_fixed_sized = grade.toFixed(NUMBER_OF_DECIMALS);

    console.log(header);
    console.log(formatInnerLine(name, header.length));
    console.log(formatInnerLine('', header.length));
    console.log(formatInnerLine(average_grade_fixed_sized, header.length));
    console.log(formatInnerLine(textGrade, header.length));
    console.log(footer);
}

const nombre = 'Andres';
const apellido1 = 'Estevez';
const apellido2  = 'Costas';

const grades = [7.9, 9.9, 1, 7, 1, 10, 1];

const full_name = `${nombre} ${apellido1} ${apellido2}`;

const average_grade = calculateAverageGrade(grades);

let grade = getTextualGrade(average_grade);

printCard(full_name, average_grade, grade);
