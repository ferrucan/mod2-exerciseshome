/**
 *
 */
nombre = 'Andres';
apellido1 = 'Estevez';
apellido2 = 'Costas';

nota_1 = '5.5';
nota_2 = '4.2';
nombrecompleto = `${nombre} ${apellido1} ${apellido2}`;
media = (parseFloat(nota_1) + parseFloat(nota_2)) / 2;
mediadecim =  media.toFixed(2);
suma2 = (nombrecompleto.length)+4;
espacioblanco = (nombrecompleto)+2;
prefijo = ' '.repeat((nombrecompleto.length/2)-1)
sufijo = ' '.repeat((nombrecompleto.length-prefijo.length-mediadecim.length)+2)

linea1 = '*'.repeat(suma2);
linea3 =' '.repeat((suma2)-2);
linea4 = `*${prefijo}${mediadecim}${sufijo}*`;

linea5 = '*'+' '.repeat(nombrecompleto.length) + 2+'*';

NotadeCorte = 5
estaAprobado = media>=NotadeCorte

if (estaAprobado) {
    NotaFinal = 'APROBADO'
}
else {
    NotaFinal = 'SUSPENSO'
}
prefnotfin =' '.repeat((linea3.length/2)-(NotaFinal.length/2))
sufnotfin = ' '.repeat((linea3.length)-(prefnotfin.length)-(NotaFinal.length))

console.log (linea1)
console.log (`* ${nombrecompleto} *`)
console.log (`*${linea3}*`)
console.log (linea4)
console.log (`*${prefnotfin}${NotaFinal}${sufnotfin}*`)
console.log (linea1)



